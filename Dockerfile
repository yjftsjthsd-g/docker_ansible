FROM python:3.8-slim
ARG version=2.8
RUN pip install ansible~=${version}.0
