# docker_ansible

Ansible docker images

This repo shoves ansible from PyPI into a container. It only exists because
there apparently aren't official ansible images (!?) and I needed it for CI.

## Use

```
docker run --rm -ti registry.gitlab.com/yjftsjthsd-g/docker_ansible:2.9 ansible -m ping localhost
```


## Versions, tags

Individual versions are tagged; ex. :2.9 will give you ansible 2.9.X (where X
should be the latest in the 2.9 series).

There is no latest tag at this writing; that's a minor TODO.


## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
image!
